const int pinLeds[] = {13,12,11};
const int pinSize = sizeof(pinLeds);

void setup()
{
  for (int i = 0; i < pinSize; i++) 
  {
    Serial.begin(9600);
    pinMode(pinLeds[i], OUTPUT);
    digitalWrite(pinLeds[i], HIGH);
    Serial.print("Ready\n");
  }
}

void loop()
{
  for (int dtOn = 1000; dtOn > 0; dtOn -= 200)
  {
    // On allume toutes les leds en meme temps pendant dtOn
    for (int i = 0; i < pinSize; i++) 
       digitalWrite(pinLeds[i], LOW);
      
    Serial.print("All LEDs on\n"); 
    delay(dtOn);

    // On les eteint  
    for (int i = 0; i < pinSize; i++) 
      digitalWrite(pinLeds[i], HIGH);
    Serial.print("All LEDs off\n"); 

    // On allume successivement chaque LED pendant dtOn, en attendant 0,1s entre chaque
    for (int i = pinSize - 1; i >= 0; i--)
    {
      delay(100);
      digitalWrite(pinLeds[i], LOW);
      Serial.print(i + 1);
      Serial.print("on\n");
      delay(dtOn);
      digitalWrite(pinLeds[i], HIGH);
      Serial.print("All LEDs off\n"); 
    }

    // On attend 1s avant de recommencer la boucle
    delay(1000); 
  }  
}
