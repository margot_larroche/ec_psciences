const int pinLeds[] = {13,12};
const int pinButton = 7;
bool buttonOn = false;

void setup()
{
  for (int i = 0; i < sizeof(pinLeds); i++) 
  {    
    pinMode(pinLeds[i], OUTPUT);
    digitalWrite(pinLeds[i], HIGH);
  }
  
  pinMode(pinButton, INPUT);
  Serial.begin(9600);
  Serial.print("Ready\n");
}

void loop()
{
  buttonOn = digitalRead(pinButton);
  if(buttonOn)
  {
    Serial.println("ON");
    digitalWrite(pinLeds[0], LOW);
    digitalWrite(pinLeds[1], HIGH);
  }
  else
  {
    Serial.println("OFF");
    digitalWrite(pinLeds[1], LOW);
    digitalWrite(pinLeds[0], HIGH);
  }
  
  delay(100);
}

