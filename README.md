# EC P-Sciences

This repository contains the code used for the scientific project complementary class.
Most of them are experiences on brain senses, but there is also some arduino stuff to play with.

Experiences are written in Python 2, some files does work with Python 3 but others don't.
Arduino stuff is written in C.

Experiences include motion induced blindness, absolute pitch, dynamic ebbinghaus and motor learning.

## Author(s):
    - Margot Larroche
    
## LICENSE :
Code is free of right. But only if you're a student though >:p