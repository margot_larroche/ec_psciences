# -*- coding: latin-1 -*-

from __future__ import print_function
from numpy import *
from math import atan, atan2
from matplotlib.pyplot import *
from scipy.interpolate import interp1d
from pickle import dump, load
from os import chdir, listdir
import sys


data_dir='/Users/Margot/Desktop/AEdata'
T_sampling=15
target_radius=15
smoothing_win=20
init_length=100


# Fonction qui lisse un vecteur en moyennant sur une fenêtre 2*smoothing_win
def smooth(vect,smoothing_win):
	smoothed_vect=zeros(len(vect))
	for i in range(len(vect)):
		smoothed_vect[i]=vect[max(i-smoothing_win,0):min(i+smoothing_win+1,len(vect))].mean()
	return smoothed_vect
	
def angle_diff(vect,vect0):	
	angle=atan2(vect[1],vect[0])-atan2(vect0[1],vect0[0])	
	if angle<-pi:
		angle+=2*pi
	elif angle>pi:
		angle-=2*pi
	return angle*180./pi		
		

# Récuperer la liste des noms des fichiers à analyser
chdir(data_dir)

files_list=[]
if len(sys.argv)>1:
	for file in sys.argv[1:]:
		files_list.append(file+'data')
else:	
	for file in listdir('.'):
		if file.endswith('data'):
			files_list.append(file)		
	
	
# Analyse fichier par fichier	
for filename in files_list:
	filein=open(filename,'r')
	data=load(filein)
	filein.close()

	analyse={}
	for block in range(len(data)):
		analyse[block]={}
		analyse[block]['initial_angle']=zeros(len(data[block]))
		analyse[block]['speeds']=zeros(len(data[block]))
		analyse[block]['distances']=zeros(len(data[block]))
		
		for trial in range(len(data[block])):
			# Recuperer les infos de trajectoire
			analyse[block][trial]={}
			t=data[block][trial]['t']
			x=data[block][trial]['x']
			y=data[block][trial]['y']
			
			# Vitesse instantanée
			analyse[block][trial]['t_half']=t[:-1]+0.5*diff(t)
			analyse[block][trial]['speed']=sqrt(diff(x)**2+diff(y)**2)*1./diff(t)
			
			# Version temporellement équidistante de x, y et t
			interp_t=arange(0,t[-1],T_sampling)
			analyse[block][trial]['interp_t']=interp_t
			analyse[block][trial]['interp_x']=interp1d(data[block][trial]['t'],x)(interp_t)
			analyse[block][trial]['interp_y']=interp1d(data[block][trial]['t'],y)(interp_t)
			
			# Version spatialement équidistante (abscisse curviligne) de x et y
			curvabs=cumsum(sqrt(diff(x)**2+diff(y)**2))
			nonzero=where(diff(curvabs)<>0)[0]
			interp_curvabs=arange(ceil(curvabs[nonzero][0]),floor(curvabs[nonzero][-1])+0.5,0.5)
			interp_curv_x=interp1d(curvabs[nonzero],x[nonzero])(interp_curvabs)
			interp_curv_y=interp1d(curvabs[nonzero],y[nonzero])(interp_curvabs)
			analyse[block][trial]['interp_curvabs_1of2']=interp_curvabs[1:-1][0::2]
			analyse[block][trial]['interp_curv_x']=interp_curv_x
			analyse[block][trial]['interp_curv_y']=interp_curv_y
			
			x_smooth=smooth(smooth(interp_curv_x,smoothing_win),smoothing_win)
			y_smooth=smooth(smooth(interp_curv_y,smoothing_win),smoothing_win)
			analyse[block][trial]['x_smooth']=x_smooth
			analyse[block][trial]['y_smooth']=y_smooth
			
			
			#	Angles (mesurés tous les deux incréments d'abscisse curviligne)
			target_dist_x=data[block][trial]['target_pos'][0]-x_smooth[1:-1][0::2]
			target_dist_y=data[block][trial]['target_pos'][1]-y_smooth[1:-1][0::2]
			target_dist=sqrt(target_dist_x**2+target_dist_y**2)
			analyse[block][trial]['discrepancy_angle']=zeros(len(target_dist))
			analyse[block][trial]['tolerance_angle']=zeros(len(target_dist))
			dx=diff(x_smooth[0::2])
			dy=diff(y_smooth[0::2])
			
			for i in range(len(target_dist)):
				if target_dist[i]==0:
					analyse[block][trial]['tolerance_angle'][i]=NaN
				else:
					analyse[block][trial]['tolerance_angle'][i]=atan(target_radius*1./target_dist[i])*180./pi
						
				if (dx[i]==0 and dy[i]==0) or target_dist[i]==0:
					analyse[block][trial]['discrepancy_angle'][i]=NaN
				else:
					analyse[block][trial]['discrepancy_angle'][i]=angle_diff([dx[i],dy[i]],[target_dist_x[i],target_dist_y[i]])

			# Valeur initiale du discrepancy angle						
			analyse[block]['initial_angle'][trial]=angle_diff([x_smooth[init_length*2]-640,y_smooth[init_length*2]-400],data[block][trial]['target_pos']-array([640,400]))
			
			# Vitesse pour atteindre la cible (normalisée par la distance départ-cible)
			analyse[block]['speeds'][trial]=data[block][trial]['target_distance']*1000./t[-1]
			
			# Distance totale pour atteindre la cible normalisée par la distance départ-cible
			analyse[block]['distances'][trial]=curvabs[-1]*1./data[block][trial]['target_distance']

							
	fileout=open(filename[:-4]+'ana','w')
	dump(analyse,fileout)
	fileout.close()			
	print filename+': OK'			
			

			
			# Valeur initiale du discrepancy angle
#			mean_angle=0
#			for i in range(5,init_length*2):
#				mean_angle+=atan2(y_smooth[i]-400,x_smooth[i]-640)
#			init_angle=mean_angle*1./(2*init_length)-atan2(data[block][trial]['target_pos'][1]-400,data[block][trial]['target_pos'][0]-640)
#			if init_angle<-pi:
#				init_angle+=2*pi
#			elif init_angle>pi:
#				init_angle-=2*pi	
#			analyse[block]['initial_angle'][trial]=init_angle*180./pi
#			#print 'position: ',[x_smooth[100],y_smooth[100]]
#			print 'angle: ',block,trial,analyse[block]['initial_angle'][trial]			
	
			
#			 Angles (mesurés tous les deux incréments d'abscisse curviligne)
#			target_dist_x=data[block][trial]['target_pos'][0]-interp_curv_x[1:-1][0::2]
#			target_dist_y=data[block][trial]['target_pos'][1]-interp_curv_y[1:-1][0::2]
#			target_dist=sqrt(target_dist_x**2+target_dist_y**2)
#			analyse[block][trial]['discrepancy_angle']=zeros(len(target_dist))
#			analyse[block][trial]['tolerance_angle']=zeros(len(target_dist))
#			dx=diff(interp_curv_x[0::2])
#			dy=diff(interp_curv_y[0::2])
#			
#			for i in range(len(target_dist)):
#				if target_dist[i]==0:
#					tolerance=NaN
#				else:
#					tolerance=atan(target_radius*1./target_dist[i])*180./pi
#						
#				if (dx[i]==0 and dy[i]==0) or target_dist[i]==0:
#					discrepancy=NaN
#				else:
#					discrepancy=atan2(target_dist_y[i],target_dist_x[i])-atan2(dy[i],dx[i])	
#					if discrepancy<-pi:
#						discrepancy+=2*pi
#					elif discrepancy>pi:
#						discrepancy-=2*pi		
#						
#				analyse[block][trial]['discrepancy_angle'][i]=discrepancy*180./pi
#				analyse[block][trial]['tolerance_angle'][i]=tolerance
			
			# Lissage de la courbe de discrepancy angle
#			discrepancy_smooth=zeros(len(target_dist))
#			for i in range(len(target_dist)):
#				discrepancy_smooth[i]=analyse[block][trial]['discrepancy_angle'][max(i-smoothing_win,0):min(i+smoothing_win+1,len(target_dist))].mean()
#			analyse[block][trial]['discrepancy_smooth']=discrepancy_smooth
				
				
							
			# Version spatialement équidistante (abscisse curviligne) et lissée de x et y (+ t correspondant)
#			curvabs=cumsum(sqrt(diff(x)**2+diff(y)**2))
#			interp_curvabs=arange(ceil(curvabs[0]),floor(curvabs[-1])+1,1)
#			interp_curv_t=interp1d(curvabs,t[1:])(interp_curvabs)
#			analyse[block][trial]['interp_curvabs_half']=interp_curvabs[:-1]+0.5*diff(interp_curvabs)
#			analyse[block][trial]['interp_curv_t_half']=interp_curv_t[:-1]+0.5*diff(interp_curv_t)
#			analyse[block][trial]['interp_curv_t']=interp_curv_t
#			analyse[block][trial]['interp_curvabs']=interp_curvabs
#			interp_curv_x=interp1d(curvabs,x[1:])(interp_curvabs)
#			interp_curv_y=interp1d(curvabs,y[1:])(interp_curvabs)
#			smooth_x=zeros(len(interp_curv_x))
#			smooth_y=zeros(len(interp_curv_y))
#			
#			for i in range(len(interp_curv_x)):
#				smooth_x[i]=interp_curv_x[max(i-4,0):min(i+5,len(interp_curv_x))].mean()
#				smooth_y[i]=interp_curv_y[max(i-4,0):min(i+5,len(interp_curv_y))].mean()
#				
#			analyse[block][trial]['smooth_x']=smooth_x
#			analyse[block][trial]['smooth_y']=smooth_y
				
				
				
			# Angles
#			analyse[block][trial]['discrepancy_angle']=zeros(len(interp_curv_t)-1)
#			analyse[block][trial]['tolerance_angle']=zeros(len(interp_curv_t)-1)
#			target_dist_x=data[block][trial]['target_pos'][0]-interp1d(interp_curv_t,smooth_x)(analyse[block][trial]['interp_curv_t_half'])
#			target_dist_y=data[block][trial]['target_pos'][1]-interp1d(interp_curv_t,smooth_y)(analyse[block][trial]['interp_curv_t_half'])
#			target_dist=sqrt(target_dist_x**2+target_dist_y**2)
#			for i in range(len(interp_curv_t)-1):
#				if target_dist[i]==0:
#					tolerance=NaN
#				else:
#					tolerance=atan(target_radius*1./target_dist[i])*180./pi
#						
#				if (diff(smooth_x)[i]==0 and diff(smooth_y)[i]==0) or target_dist[i]==0:
#					discrepancy=NaN
#				else:
#					discrepancy=atan2(target_dist_y[i],target_dist_x[i])-atan2(diff(smooth_y)[i],diff(smooth_x)[i])	
#					if discrepancy<-pi:
#						discrepancy+=2*pi
#					elif discrepancy>pi:
#						discrepancy-=2*pi
#				analyse[block][trial]['discrepancy_angle'][i]=discrepancy*180./pi
#				analyse[block][trial]['tolerance_angle'][i]=tolerance

			
			
#			# Angles
#			analyse[block][trial]['discrepancy_angle']=zeros(len(t)-1)
#			analyse[block][trial]['tolerance_angle']=zeros(len(t)-1)
#			target_dist_x=data[block][trial]['target_pos'][0]-interp1d(t,x)(analyse[block][trial]['t_half'])
#			target_dist_y=data[block][trial]['target_pos'][1]-interp1d(t,y)(analyse[block][trial]['t_half'])
#			target_dist=sqrt(target_dist_x**2+target_dist_y**2)
#			for i in range(len(t)-1):
#				if target_dist[i]<target_radius:
#				if target_dist[i]==0:
#					tolerance=NaN
#				else:
#					tolerance=atan(target_radius*1./target_dist[i])*180./pi
#						
#				if (diff(x)[i]==0 and diff(y)[i]==0) or target_dist[i]<target_radius:
#				if (diff(x)[i]==0 and diff(y)[i]==0) or target_dist[i]==0:
#					discrepancy=NaN
#				else:
#					discrepancy=atan2(target_dist_y[i],target_dist_x[i])-atan2(diff(y)[i],diff(x)[i])	
#					if discrepancy<-pi:
#						discrepancy+=2*pi
#					elif discrepancy>pi:
#						discrepancy-=2*pi
#				analyse[block][trial]['discrepancy_angle'][i]=discrepancy*180./pi
#				analyse[block][trial]['tolerance_angle'][i]=tolerance

			# Identifier la phase de mouvement initial? Si tant est qu'elle existe			
			
