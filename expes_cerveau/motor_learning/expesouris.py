# -*- coding: latin-1 -*-

from __future__ import print_function
from os import getcwd
data_dir=getcwd()
import pygame
from pygame.locals import *
from numpy import *
from random import uniform,shuffle
from matplotlib.pyplot import *
from pickle import dump
from datetime import datetime
from os import chdir
import sys

cursor_radius=3
target_radius=15
fontsize_small=80
fontsize_big=100
cursor_col=[0,0,0]
cursor_col_wait=[220,220,220]
target_col=[255,0,0]
bg_col=[255,255,255]
font_col=[0,0,0]
T_sampling=14

cursor_angles=[0,0,60,60,-60,-60]
if len(sys.argv)>1 and sys.argv[1]=='demo':
	target_angles=[[180]*5,range(0,360,72)]*3
	target_distances=[[200,250,300,200,300],[250]*5]*3	
else:	
	target_angles=[[180]*30,range(0,360,12)]*3
	target_distances=[[200,250,300]*10,[250]*30]*3



def wait(dur=None,mouse=False,click=False,kreturn=False):
	pygame.event.clear()
	ticks0 = pygame.time.get_ticks()
	while True:
		if dur != None and pygame.time.get_ticks() > ticks0 + dur:
			return
		for ev in pygame.event.get():
			if ev.type == QUIT or (ev.type == KEYDOWN and ev.key == K_ESCAPE):
				raise Exception
			if mouse==True and ev.type == MOUSEMOTION:
				return pygame.time.get_ticks()-ticks0
			if kreturn==True and ev.type == KEYDOWN and ev.key ==K_RETURN:
				return
			if click==True and ev.type==MOUSEBUTTONDOWN:
				return


# Angle est donné en degrés
# !! Tient compte du fait que les ordonnées sont indicées de haut en bas => on prend -theta pour compenser		
def rotate(coord,theta):
		theta_rad=-1*theta*pi/180
		return array([int(coord[0]*cos(theta_rad)-coord[1]*sin(theta_rad)), int(coord[0]*sin(theta_rad)+coord[1]*cos(theta_rad))])


def win_update(cursor_pos=None,target_pos=None,cursor_color=cursor_col,text_big=None,text_small=None):
	window.fill(bg_col)
	if target_pos<>None:
		pygame.draw.circle(window,target_col,target_pos,target_radius)
	if cursor_pos<>None:
		pygame.draw.circle(window,cursor_color,cursor_pos,cursor_radius)
	if text_big<>None:
		image=font_big.render(text_big, True, font_col, bg_col)
		w,h=image.get_size()
		window.blit(image,[W/2-w/2,H/2-h])
	if text_small<>None:
		image=font_small.render(text_small, True, font_col, bg_col)
		w,h=image.get_size()
		window.blit(image,[W/2-w/2,H/2+h])	
	pygame.display.flip()


def run_trial(theta,target_pos):

	cursor_pos=array([W/2,H/2])
	target_pos=array(target_pos)
	trajectory=[list(cursor_pos)]
	time=[0]
	
	win_update(cursor_pos,cursor_color=cursor_col_wait)
	wait(click=True)
	win_update(cursor_pos)
	wait(uniform(500,2000))
	win_update(cursor_pos,target_pos)
	t0 = pygame.time.get_ticks()
	# Juste pour supprimer le problème de saut de la souris au départ... et permet de ne pas scanner pour rien tant que la souris n'a pas bougé
	RT=wait(mouse=True)
	pygame.mouse.get_rel()
	wait(T_sampling)
	
	while ((cursor_pos-target_pos)**2).sum()>target_radius**2 or (cursor_pos<>trajectory[-1*min(2,len(time))]).any():
		time+=[pygame.time.get_ticks()-t0-RT]
		delta_pos=pygame.mouse.get_rel()
		rotated_delta=rotate(delta_pos,theta)
		cursor_pos=array([[0,0],array([[W,H],cursor_pos+rotated_delta]).min(axis=0)]).max(axis=0)
		trajectory+=[list(cursor_pos)]
		win_update(cursor_pos,target_pos)
		wait(max(T_sampling-pygame.time.get_ticks()+t0+RT+time[-1],0))
			
	return time,trajectory,RT
	
	

try:
	subject_name = raw_input('Nom: ')
	pygame.init()
	font_big = pygame.font.Font(None, fontsize_big)
	font_small = pygame.font.Font(None, fontsize_small)
	window = pygame.display.set_mode([0, 0], FULLSCREEN | DOUBLEBUF | HWSURFACE)
	W, H = window.get_size()
	pygame.mouse.set_visible(False)
	pygame.event.set_grab(True)
	
	target_list=[]
	nblocks=len(cursor_angles)
	for block in range(nblocks):
		ntrials=len(target_distances[block])
		shuffle(target_distances[block])
		shuffle(target_angles[block])
		target_list+=[[array([W/2,H/2])+rotate(array([0,1])*target_distances[block][i],target_angles[block][i]) for i in range(ntrials)]]
	
	date_time=datetime.now()
	t_begin=pygame.time.get_ticks()
	data_struct={}
	
	win_update(text_big='Bouton entree pour demarrer')
	wait(kreturn=True)
	
	for block in range(nblocks):
		data_struct[block]={}
		for trial in range(len(target_list[block])):
			data_struct[block][trial]={}
			time,trajectory,RT=run_trial(cursor_angles[block],target_list[block][trial])
			data_struct[block][trial]['t']=array(time)
			data_struct[block][trial]['x']=array(trajectory)[:,0]
			data_struct[block][trial]['y']=H-array(trajectory)[:,1] # !! On se ramène à un repère orienté vers le haut
			data_struct[block][trial]['RT']=RT
			data_struct[block][trial]['cursor_angle']=cursor_angles[block]
			data_struct[block][trial]['target_angle']=target_angles[block][trial]
			data_struct[block][trial]['target_distance']=target_distances[block][trial]
			data_struct[block][trial]['target_pos']=array([target_list[block][trial][0],H-target_list[block][trial][1]]) # !! idem
			
	exp_dur_s=round(0.001*(pygame.time.get_ticks()-t_begin))
	exp_dur_min=str(int(exp_dur_s/60))+' min '+str(int(exp_dur_s%60))+'s'
	win_update(text_big="C'est fini!",text_small="Merci =)")
	wait(kreturn=True)
	

finally:
	pygame.quit()
	
	date=str(date_time.date())
	time=str(date_time.time())[0:8]
	
	chdir(data_dir)
	
	outpy=open(subject_name+date[6:]+'data','w')
	dump(data_struct,outpy)
	outpy.close()
	
	outtxt=open(subject_name+date[6:]+'.txt','w')
	print('Date: '+date,'Heure: '+time, 'Sujet: '+subject_name, 'Duree: '+exp_dur_min, sep='\n', file=outtxt)
	print('\nDebriefing:\n'+raw_input('Debriefing:'), file=outtxt)
	print('\nRemarques:\n'+raw_input('Remarques:'), file=outtxt)
	outtxt.close()
		

# OK ça marche, par contre problème très con: quand la souris arrive au bord de l'écran elle s'arrête même si on continue à lui demander d'avancer, ce qui fait que la souris modifiée s'arrête en plein milieu de l'écran... Solution la plus facile: réduire la taille de la fenêtre en conséquence
# OK problème carrément supprimé avec set_grab qui permet à une souris virtuelle de continuer à avancer même si la souris affichée était en fait bloquée au bord de l'écran.
# Mais autre problème qui apparaît alors: le premier mouvement de la souris est un énorme saut qui ne correspond pas du tout au mouvement de la main! Comment ça se fait??
# Parait qu'en mode souris virtuelle python force la souris à revenir au milieu de l'écran... au début seulement? Mais en fait quand je l'ai moi-même forcée à revenir au milieu, fait quand même un saut: tout se passe comme si pygame n'avait pas remarqué que j'avais déjà renvoyé la souris au milieu et avait donc programmé le saut comme si elle se trouvait encore dans son ancienne position.
# Problème réglé en "avalant" le premier saut dans la fonction wait AVANT de lancer la boucle