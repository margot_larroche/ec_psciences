Il s'agit d'une expérience sur l'apprentissage moteur. On demande à l'utilisateur une tâche très simple : déplacer le curseur du centre de l'écran vers le disque rouge qui apparaît quelque-part sur l'écran. Et ce ... plein plein de fois (très ennuyeux à faire). A un moment, le curseur se met à se comporter bizarrement (c'était la partie marrante à coder). Le but est de voir comment l'utilisateur s'y adapte progressivement au fil des essais (de manière pas forcément consciente : il ne comprend pas nécessairement intellectuellement ce qui est arrivé au curseur mais sa main fait le job, c'est ça qui est intéressant).

Pour plus d'explications sur l'intérêt scientifique du truc : https://sites.google.com/site/exphum/projets/motor-learning



Concrètement :

- Lancer l'expérience en tapant "python expesouris.py" (marche sans bug en python 2.7 sur mac OS 10.11, pas testé pour les autres versions).

- Taper le nom de l'utilisateur en ligne de commande

- À chaque essai, on commence par un écran blanc avec un point grisé au centre = le curseur. On fait clic droit pour lancer l'essai, le curseur devient alors noir. Puis un disque rouge apparaît sur l'écran -> il faut déplacer le curseur vers le disque le plus vite possible. Quand on l'a atteint, l'essai se termine. Puis rebelote plein de fois (taper "python expesouris.py demo" pour une version plus courte).

- A la fin, on demande de renseigner du texte en ligne de commande ("Debriefing" puis "Remarques"). (étape superflue, pourra être enlevé du code)

- Le programme enregistre la trajectoire du curseur à chaque essai et la sauve dans un fichier binaire

- analyse.py et plots.py servent ensuite à l'analyse des résultats mais /!\ pas testés depuis longtemps, très fouillis, il y a sûrement des choses à modifier (notamment les chemins d'accès).