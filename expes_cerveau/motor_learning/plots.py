# -*- coding: latin-1 -*-

from numpy import *
from matplotlib.pyplot import *
from pickle import dump, load
from os import chdir, listdir
import sys


data_dir='/Users/Margot/Desktop/AEdata'
plots_dir='/Users/Margot/Desktop/AEplots'
T_sampling=15
target_radius=15
colors=['r','g','b','y','c']

# Récuperer la liste des noms des fichiers à analyser
chdir(data_dir)
files_list=[]

if len(sys.argv)>1:
	files_list=sys.argv[1:2]
else:
	for file in listdir('.'):
		if file.endswith('data'):
			files_list.append(file[:-4])
	
	
# Plots fichier par fichier	
for filename in files_list:
	chdir(data_dir)
	filein=open(filename+'data','r')
	data=load(filein)
	filein.close()
	filein=open(filename+'ana','r')
	ana=load(filein)
	filein.close()
	chdir(plots_dir)
	
	
	# Tracer les trajectoires (+ points à intervalles réguliers) 5 par 5, 1 figure de 6 plot par bloc
#	if len(sys.argv)<4 or sys.argv[3]=='traj':
#		for block in range(len(data)):	
#			figure(figsize=[15,10])
#			for i in range(6):
#				subplot(2,3,i+1)
#				for j in range(5):
#					plot(data[block][5*i+j]['x'],data[block][5*i+j]['y'],colors[j],label=`5*i+j+1`)
#					plot(ana[block][5*i+j]['interp_x'][0::5],ana[block][5*i+j]['interp_y'][0::5],colors[j]+'o',markersize=4,markeredgecolor=colors[j])
#				axis([240,1040,0,800])
#				legend(title='Trial n',labelspacing=0.01)	
#			savefig(filename+'traj-b'+str(block+1)+'.pdf')
		
		
	# Tracer les angles selon le même schéma
#	if len(sys.argv)<4 or sys.argv[3]=='angles':	
#		for block in range(len(data)):	
#			figure(figsize=[15,10])
#			for i in range(6):
#				subplot(2,3,i+1)
#				for j in range(5):
#					plot(ana[block][5*i+j]['interp_curvabs_1of2'],ana[block][5*i+j]['discrepancy_angle'],colors[j],label=`5*i+j+1`)
#					plot(ana[block][5*i+j]['interp_curvabs_1of2'],ana[block][5*i+j]['tolerance_angle'],colors[j]+'--')
#					plot(ana[block][5*i+j]['interp_curvabs_1of2'],-1*ana[block][5*i+j]['tolerance_angle'],colors[j]+'--')
#				axis([0,600,-180,180])
#				xlabel('Curvilinear abscissa (pixels)')
#				ylabel('Discrepancy and tolerance angles (degrees)')
#				legend(title='Trial n',labelspacing=0.01)	
#			savefig(filename+'angles-b'+str(block+1)+'.pdf')


# Idem pour les vitesses instantanées
#	if len(sys.argv)<4 or sys.argv[3]=='instspeed':
#		for block in range(len(data)):	
#			figure(figsize=[15,10])
#			for i in range(6):
#				subplot(2,3,i+1)
#				for j in range(5):
#					plot(ana[block][5*i+j]['t_half'],ana[block][5*i+j]['speed'],colors[j],label=`5*i+j+1`)
#				legend(title='Trial n',labelspacing=0.01)
#				xlabel('Time (ms)')
#				ylabel('Speed (pixels/ms)')
#			savefig(filename+'instspeed-b'+str(block+1)+'.pdf')

			
	
	# Tracer l'évolution de l'angle initial pour chaque bloc
#	if len(sys.argv)<4 or sys.argv[3]=='init':
#		figure()
#		for block in range(len(data)):
#			plot(range(1,len(ana[block]['initial_angle'])+1), ana[block]['initial_angle'],label=`block+1`)
#		axis([0,30,-180,180])
#		xlabel('Trial n')
#		ylabel('Initial discrepancy angle (degrees)')
#		legend(title='Block n',labelspacing=0.01)	
#		savefig(filename+'initial'+'.pdf')
		
		
	# Tracer les % de trajectoire passés dans chaque angle, par groupes de 10 trials
	if len(sys.argv)<4 or sys.argv[3]=='hist':
		for block in range(len(data)):
			figure(figsize=[20,5])
			for i in range(3):
				subplot(1,3,i+1)
				discrepancy=[]
				for j in range(10):
					trialvalues=ana[block][10*i+j]['discrepancy_angle']
					discrepancy+=list(trialvalues[isnan(trialvalues)==False])
				hist(discrepancy,bins=arange(-180,181,8),normed=True)
				axis([-180,180,0,0.025])
				xlabel('Angle (degrees)')
				ylabel('Frequency')
				title('Trial '+`i*10+1`+' to '+`(i+1)*10`+' (mean: '+`round(array(discrepancy).mean(),1)`+', sd: '+`round(std(discrepancy),1)`+ ')')
			savefig(filename+'hist-b'+`block+1`+'.pdf',bbox_inches='tight')


	# Tracer l'évolution de la vitesse d'exécution pour chaque bloc
#	if len(sys.argv)<4 or sys.argv[3]=='speeds':
#		figure()
#		for block in range(len(data)):
#			plot(range(1,len(ana[block]['speeds'])+1), ana[block]['speeds'],label=`block+1`)
#		axis([0,30,0,1000])
#		xlabel('Trial n')
#		ylabel('Mean speed (pixel/s)')
#		legend(title='Block n',labelspacing=0.01)	
#		savefig(filename+'speeds'+'.pdf')
		
		
	# Tracer l'évolution de la distance totale parcourue pour chaque bloc
#	if len(sys.argv)<4 or sys.argv[3]=='dist':
#		figure()
#		for block in range(len(data)):
#			plot(range(1,len(ana[block]['distances'])+1), ana[block]['distances'],label=`block+1`)
#		axis([0,30,0,10])
#		xlabel('Trial n')
#		ylabel('Trajectory length / target distance')
#		legend(title='Block n',labelspacing=0.01)	
#		savefig(filename+'distances'+'.pdf')	

	
	# Tracer l'erreur d'angle initiale en fonction de la direction de la cible	
#	if len(sys.argv)<4 or sys.argv[3]=='generalang':
#		figure()
#		for block in [1,3,5]:
#			target_angles=[data[block][trial]['target_angle'] for trial in range(30)]
#			plot(target_angles, ana[block]['initial_angle'],'o',label=`block+1`)
#			axis([0,360,-180,180])
#		xlabel('Target direction (degrees)')
#		ylabel('Initial discrepancy angle (degrees)')
#		legend(title='Block n',labelspacing=0.01)	
#		savefig(filename+'generalang'+'.pdf')	
		
		
	# Tracer la vitesse d'exécution en fonction de la direction de la cible	
#	if len(sys.argv)<4 or sys.argv[3]=='generalspeed':
#		figure()
#		for block in [1,3,5]:
#			target_angles=[data[block][trial]['target_angle'] for trial in range(30)]
#			plot(target_angles, ana[block]['speeds'],'o',label=`block+1`)
#			axis([0,360,0,800])
#		xlabel('Target direction (degrees)')
#		ylabel('Mean speed (pixel/s)')
#		legend(title='Block n',labelspacing=0.01)	
#		savefig(filename+'generalspeed'+'.pdf')
		
		
	# Tracer la taille de la trajectoire en fonction de la direction de la cible	
#	if len(sys.argv)<4 or sys.argv[3]=='generaldist':
#		figure()
#		for block in [1,3,5]:
#			target_angles=[data[block][trial]['target_angle'] for trial in range(30)]
#			plot(target_angles, ana[block]['distances'],'o',label=`block+1`)
#			axis([0,360,0,5])
#		xlabel('Target direction (degrees)')
#		ylabel('Trajectory length / target distance')
#		legend(title='Block n',labelspacing=0.01)	
#		savefig(filename+'generaldist'+'.pdf')		
			
			
	print filename+': OK'		
			
			
			
#	# Tracer les angles selon le même schéma
#	if len(sys.argv)<4 or sys.argv[3]=='angles':	
#		for block in range(len(data)):	
#			figure(figsize=[15,10])
#			for i in range(6):
#				subplot(2,3,i+1)
#				for j in range(5):
#					plot(ana[block][5*i+j]['t_half'],ana[block][5*i+j]['discrepancy_angle'],colors[j],label=`5*i+j+1`)
#					plot(ana[block][5*i+j]['t_half'],ana[block][5*i+j]['tolerance_angle'],colors[j]+'--')
#				axis([0,1000,-180,180])
#				xlabel('Time (ms)')
#				ylabel('Discrepancy and tolerance angles (degrees)')
#				legend(title='Trial n',labelspacing=0.01)	
#			savefig(filename+'angles-b'+str(block+1)+'.pdf')		
			
			