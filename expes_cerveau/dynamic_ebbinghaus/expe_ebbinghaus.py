# -*- coding: latin-1 -*-


from os import getcwd
main_path=getcwd()
sep=' '
exp_name='ebbinghaus'
fontsize_small=60
fontsize_big=80
font_col=[0,0,0]


import sys, random, pygame
from pygame.locals import *
from math import *
import numpy as np
from datetime import datetime

main_pm = {
'circles' : [(1,0),(-1,0),(np.cos(np.pi/3),np.sin(np.pi/3)),(-np.cos(np.pi/3),-np.sin(np.pi/3)),(-np.cos(np.pi/3),np.sin(np.pi/3)),(np.cos(np.pi/3),-np.sin(np.pi/3))],
'fixation_pos' : (-np.cos(np.pi/3),np.sin(np.pi/3)),
'circle_scale' : [0.1,0.4],
'circle_radius' : [10,70],
'center_radius' : 20,
'fixation_radius' : 4,
'cycle_speed' : 0.5,
'trajectory' : [np.array([-0.4,0.4]),np.array([0.4,-0.4])],
'cycle_shape' : 'linear', 
'bg_color' : (255, 255, 255),
'circle_color' : (0, 0, 0),
'fixation_color': (255, 0, 0),
't_mov': 6,
't_fix':1.5,
'name': 'standard'
}

variants = [
{'name': 'standard'},
{'name': 'vitesse', 'cycle_speed': 1},
{'name': 'couleurs_inversées', 'bg_color': [0,0,0], 'circle_color': [255, 255, 255]},
{'name': 'mouvement_ronds', 'circle_radius': [20,20], 'circle_scale' : [0.1,0.4]}
]
n_var=len(variants)
variant_pairs=[[variants[i1],variants[i2]] for i1 in range(n_var) for i2 in range(i1)+range(i1+1,n_var)]
main_fixation_pos=np.array(main_pm['fixation_pos'])*main_pm['circle_scale'][0]+main_pm['trajectory'][0]

demo=False
n_repetitions = 1 # 3
full_screen = False
window_size = (1024, 768)


window_center = (window_size[0]/2.0, window_size[1]/2.0)
window_scale = min(window_size)/2.0
def coord(real):
	"""takes real coordinates, returns pixel coordinates"""
	return (int(round(real[0]*window_scale + window_center[0])),\
			int(round(-real[1]*window_scale + window_center[1])))
			

def wait(dur=None,return_keys=[]):
	pygame.event.clear()
	ticks0 = pygame.time.get_ticks()
	while True:
		if dur != None and pygame.time.get_ticks() > ticks0 + dur*1000:
			return None
		for ev in pygame.event.get():
			if ev.type == QUIT or (ev.type == KEYDOWN and ev.key == K_ESCAPE):
				raise Exception
			if ev.type == KEYDOWN and ev.key in return_keys:
				return ev.key
				

def win_update(window,fixation_pos=None,fixation_size=3,text_col=(200,200,200),background_col=(0,0,0),fixation_col=(255, 255, 255),text_big=None,text_small=None, max_nwords_big=5, max_nwords_small=10):
	window.fill(background_col)
	if fixation_pos<>None:
		pygame.draw.circle(window,fixation_col,fixation_pos,fixation_size)
	if text_big<>None:
		write_long_text(window, text_big, font_big, max_nwords=max_nwords_big, text_col=text_col, background_col=background_col, offset=(0,0.25))
	if text_small<>None:
		write_long_text(window, text_small, font_small, max_nwords=max_nwords_small, text_col=text_col, background_col=background_col, offset=(0,-0.25))
	pygame.display.flip()	


def write_long_text(window,text,font,max_nwords=5,text_col=(255,255,255),background_col=(0,0,0),offset=(0,0)):
	words=text.split(' ')
	n_rows=int(np.ceil(len(words)*1./max_nwords))
	for irow in range(n_rows):
		image=font.render(' '.join(words[irow*max_nwords:min((irow+1)*max_nwords,len(words))]), True, text_col, background_col)
		w,h=image.get_size()
		window.blit(image,np.array(coord(offset))+np.array([-0.5*w,-(0.5-irow)*h]))


def cycle_state(time,speed,bounds=[0,1],shape='linear'):
	dist=time*speed
	if shape=='linear':
		state=dist%2
		if state>=1:
			state=2-state
	elif shape=='sin':	
		state=np.sin(2*np.pi*dist)	
	return state*(bounds[1]-bounds[0])+bounds[0]
			

# graphics initializations
try:
	# Experiment info
	date=str(datetime.now().date())
	time=str(datetime.now().time())[:8]
	time='-'.join([time[:2],time[3:5],time[6:8]])
	investigator_name=raw_input("\nPRÉNOM EXPÉRIMENTATEUR/TRICE : ")
	subject_name=raw_input("PRÉNOM COBAYE : ")
	if (len(investigator_name)==0) and (len(subject_name)==0):
		filename=''
		demo = True
		n_repetitions=1
		print "\n###################\n /!\ MODE DÉMO /!\ \n###################\n"
	else:
   		filename=main_path+'_'.join([exp_name,investigator_name,subject_name,date,time])+'.txt'
		with open(filename,'w') as f:
			f.write(sep.join(['Numéro_essai','Version1','Version2','Choix'])+'\n')
	trials=variant_pairs * n_repetitions
	random.shuffle(trials)


	pygame.init()
	font_big = pygame.font.Font(None, fontsize_big)
	font_small = pygame.font.Font(None, fontsize_small)
	if full_screen:
		surf = pygame.display.set_mode(window_size, HWSURFACE | FULLSCREEN | DOUBLEBUF)
	else:
		surf = pygame.display.set_mode(window_size)

	for itrial in range(len(trials)):
		win_update(surf, fixation_pos=coord(main_fixation_pos), fixation_size=main_pm['fixation_radius'], text_big='ESSAI '+str(itrial+1)+'/'+str(len(trials)), text_small='Appuyer sur entree pour demarrer', fixation_col=main_pm['fixation_color'])
		wait(return_keys=[K_RETURN])

		for ivar in range(2):
			pm=main_pm.copy()
			pm.update(trials[itrial][ivar])
			win_update(surf,fixation_pos=coord(np.array(pm['fixation_pos'])*pm['circle_scale'][0]+pm['trajectory'][0]), fixation_size=pm['fixation_radius'], text_big='Version '+str(ivar+1), text_small='Suivre le point rouge des yeux', fixation_col=pm['fixation_color'])
			wait(dur=2.5)
			
			abort=False
			t0 = pygame.time.get_ticks()			
			while not(abort):
				for event in pygame.event.get():
					if (event.type==QUIT) or ((event.type == KEYDOWN) and (event.key == K_ESCAPE)): raise Exception()
	
				surf.fill(pm['bg_color'])
				t = (pygame.time.get_ticks() - t0)/1000.0
				t_mov=max(t-pm['t_fix'],0)
				if t>(pm['t_mov']+pm['t_fix']): abort=True
	
				circle_scale=cycle_state(t_mov,pm['cycle_speed'],bounds=pm['circle_scale'],shape=pm['cycle_shape'])
				pos_shift=cycle_state(t_mov,pm['cycle_speed'],bounds=pm['trajectory'],shape=pm['cycle_shape'])
				circle_radius=int(np.round(cycle_state(t_mov,pm['cycle_speed'],bounds=pm['circle_radius'],shape=pm['cycle_shape'])))
				# Remote circles
				for circ in pm['circles']:
					c = np.array([circle_scale*circ[0], circle_scale*circ[1]])+pos_shift
					pygame.draw.circle(surf, pm['circle_color'], coord(c), circle_radius)
	
				# Central circle
				pygame.draw.circle(surf, pm['circle_color'], coord(np.array([0,0])+pos_shift), pm['center_radius'])
				
				# Fixation point
				pygame.draw.circle(surf, pm['fixation_color'], coord(np.array(pm['fixation_pos'])*circle_scale+pos_shift), pm['fixation_radius'])
				pygame.display.flip()
				
		win_update(surf,text_big='Dans quelle version la taille du disque central variait-elle le plus ?', text_small='Appuyer sur 1 ou 2', max_nwords_big=4)	
		key=wait(return_keys=[K_1,K_2,K_a]) #key=wait(return_keys=[K_1,K_2])
		if not(demo):
			with open(filename,'a') as f:
				f.write(sep.join([str(itrial+1), trials[itrial][0]['name'], trials[itrial][1]['name'], str(1 if key==K_1 else 2)])+'\n')



finally:
	pygame.quit()
