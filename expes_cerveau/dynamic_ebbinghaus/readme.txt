"Dynamic Ebbinghaus" est le nom d'une illusion d'optique. expe_ebbinghaus.py est un programme d'expérience pour montrer différentes versions de l'illusion et demander à l'utilisateur de les comparer.

Infos sur l'illusion :
https://michaelbach.de/ot/cog-EbbingDyn/index.html
http://illusionoftheyear.com/2014/05/the-dynamic-ebbinghaus/