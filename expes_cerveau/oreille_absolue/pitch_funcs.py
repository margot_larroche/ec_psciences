# -*- coding: latin1 -*-

"""
Notes :
    
L'octave 3 est celle du milieu sur un piano
La 440 Hz = la_3 
   
En ton pur, avec une ampl de 1, en dessous du do_1 environ on entend plus rien même avec le volume à fond 

Volume sonore : pas clair quelles amplitudes sont autorisées par le module sounddevice.
Il semblerait que ça ne soit pas défini par sounddevice lui-même mais par la librairie qu'il appelle (PortAudio).
Du coup ça dépend de l'ordi : apparemment sur un mac on peut envoyer des amplitudes >1 et ça marche
(augmente le volume sans distortion, comme attendu) jusqu'à un certain point : c'est ce que j'ai constaté sur le mien.
Par contre sur d'autres systèmes ça merde dès qu'on dépasse 1
Cf https://github.com/spatialaudio/python-sounddevice/issues/23
"""


import numpy as np
import sounddevice as sd
import matplotlib
matplotlib.use("TkAgg") # To avoid crashing Tkinter
import matplotlib.pyplot as plt


ref_freqs={'la_3_440':440, 'do_3_440':440./(2.**(9./12.)), 'do_2_440': 440./(2.**((12*1+9)/12.))}
ampl_funcs={'linear_on_off': lambda t: np.minimum(2-4*np.abs(t-0.5),1)}
gaussian_func=lambda x: np.exp(-0.5*x**2)            
shepard_windows={'gaussian': lambda log_f: gaussian_func(4*log_f), 'flat': lambda log_f: log_f-log_f+1.}            
default_samp_freq=48000  
default_shepard_octaves=range(-6,10)   
with open('rgb_color_list.txt','r') as f:
        colors_dict=eval('{'+f.read()+'}')  
model_colors=[colors_dict[name] for name in ['dark_blue','light_red','dark_green','yellow','orange','pink','purple']]        
pitch_colors=[colors_dict[name] for name in ['pink','purple','dark_blue','light_blue','cyan','green_blue','dark_green','light_green','yellow','orange','light_red','magenta']]            
pitch_ids=range(12)
pitch_names=['DO','DO#\nREb','RE','RE#\nMIb','MI','FA','FA#\nSOLb','SOL','SOL#\nLAb','LA','LA#\nSIb','SI']


def play_tone(tone_freq,tone_dur,sampling_freq=default_samp_freq,ampl_func=ampl_funcs['linear_on_off'],ampl_factor=1.):
    times=np.arange(0,tone_dur+1./sampling_freq,1./sampling_freq)
    sound_vec=np.sin(times*2.*np.pi*tone_freq)*ampl_func(times*1./tone_dur)*ampl_factor
    sd.play(sound_vec,sampling_freq,blocking=True)
    
    
def play_shepard(semitone,tone_dur,sampling_freq=default_samp_freq,ampl_func=ampl_funcs['linear_on_off'],ampl_factor=1.,ref_freq=ref_freqs['do_3_440'],octaves=default_shepard_octaves,window_func=shepard_windows['gaussian']):
    octaves=np.array(octaves)
    times=np.arange(0,tone_dur+1./sampling_freq,1./sampling_freq)
    window_width=0.5*(max(octaves)-min(octaves))*12
    window_center=0.5*(max(octaves)+min(octaves))*12
    harmonics=semitone+octaves*12
    relative_ampls=window_func((harmonics-window_center)*1./window_width).reshape(-1,1)
    #tone_vecs=np.array([np.sin(times*2.*np.pi*semitone_to_freq(semitone+oc*12,ref_freq))*ampl_func(times*1./tone_dur)*ampl_factor for oc in octaves])
    tone_vecs=np.array([np.sin(times*2.*np.pi*semitone_to_freq(h,ref_freq)) for h in harmonics])
    sound_vec=np.sum(tone_vecs*relative_ampls,axis=0)*1./relative_ampls.sum()*ampl_func(times*1./tone_dur)*ampl_factor
    sd.play(sound_vec,sampling_freq,blocking=True)
    
    
def semitone_to_freq(semitone,ref_freq):
    return ref_freq*2**(semitone/12.)    
   
    
def shepard_sequence(semitones,tone_dur,octaves=default_shepard_octaves,ampl_factor=1.,window_func=shepard_windows['gaussian']):
    for tone in semitones:
        play_shepard(tone,tone_dur,octaves=octaves,ampl_factor=ampl_factor,window_func=window_func)
    
    
def circular_correction(ref_vals,target_vals,period=12):
    distances=(np.array(target_vals)-np.array(ref_vals))/(0.5*period)
    correction = np.zeros(distances.shape)
    correction[np.abs(distances)<=1]=0
    correction[distances>1]=-(np.ceil(distances[distances>1])-1)
    correction[distances<-1]=-(np.floor(distances[distances<-1])+1)       
    return np.array(target_vals)+correction*period 
        

def resp_xy_plot(inputs,responses):
    fig=plt.figure();
    plt.plot([0,12],[0,12],'k') # Identity line
    plt.plot(np.arange(0,12,0.01),np.round(np.arange(0,12,0.01)),color=model_colors[1]) # Response curve for a perfect absolute pitch tuned at 440 Hz
    plt.plot(inputs,circular_correction(inputs,responses),'o',color=model_colors[0]) # Data points
    #plt.axis([0,12,-6,18]) # Max distance to input value : 6 (because of circular correction)  
    #plt.axis('equal')
    plt.xticks(range(13))
    plt.yticks(range(-6,19))
    plt.axis([0,12,-6,18]) # Max distance to input value : 6 (because of circular correction) 
    plt.axis('equal')
    plt.grid()
    return fig
    
def distance_hist(inputs,responses,models=['data']):
    bins=np.arange(-6.5,7,1)
    fig=plt.figure()
    for imodel in range(len(models)):
        sim_resps=simulate_resp(inputs,responses,models[imodel])
        plt.hist(circular_correction(inputs,sim_resps)-inputs,bins=bins,color=model_colors[imodel],alpha=0.5,normed=True)  
    return fig

def confusion_matrix(inputs,responses,models=['data']):
    fig=plt.figure()
    for imodel in range(len(models)):
        sim_resps=simulate_resp(inputs,responses,models[imodel])  
        conf_mat=np.zeros([12,12])
        for in_val in range(12):
            for out_val in range(12):
                conf_mat[in_val,out_val]=np.sum((np.round(inputs)==in_val) & (sim_resps==out_val))
        plt.subplot(len(models),1,imodel+1) 
        plt.imshow(conf_mat)
        plt.title(models[imodel])
    return fig        
    
def category_hists(inputs,responses,bin_width=0.2,normed=False,models=['data']):
    bins=np.arange(0,12+bin_width,bin_width)
    resp_range=range(12)
    fig=plt.figure()
    for imodel in range(len(models)):
        sim_resps=simulate_resp(inputs,responses,models[imodel])    
        plt.subplot(len(models),1,imodel+1)  
        for irep in range(len(resp_range)):
            plt.hist(inputs[sim_resps==resp_range[irep]],bins=bins,normed=normed,alpha=0.5,color=pitch_colors[irep])
            plt.axvline(resp_range[irep],color=pitch_colors[irep])
        #plt.legend(resp_range)
        plt.xticks(resp_range)
        plt.title(models[imodel])
    return fig     

def resp_hist(inputs,responses,models=['data']):
    fig=plt.figure() 
    for imodel in range(len(models)):
       plt.hist(simulate_resp(inputs,responses,models[imodel]),bins=np.arange(-0.5,12,1),color=model_colors[imodel])  
    plt.legend(['data']+models)   
    return fig
    
    
def psychometric_curves(inputs,responses,bin_width=0.1,models=['data']):
    bins=np.arange(-0.5*bin_width,12+bin_width,bin_width)
    bin_centers=0.5*(bins[:-1]+bins[1:])
    resp_range=pitch_ids
    fig=plt.figure()
    for imodel in range(len(models)):
        sim_resps=simulate_resp(inputs,responses,models[imodel]) 
        curves=np.zeros([len(resp_range),len(bins)-1])
        for ibin in range(len(bins)-1):
            in_bin=((inputs>=bins[ibin]) & (inputs<bins[ibin+1]))
            for irep in range(len(resp_range)):
                curves[irep,ibin]=np.sum(sim_resps[in_bin]==resp_range[irep])*1./in_bin.sum()
        plt.subplot(len(models),1,imodel+1)
        for irep in range(len(resp_range)):
            plt.plot(bin_centers,curves[irep],color=pitch_colors[irep],linewidth=2)
            plt.axvline(resp_range[irep],color=pitch_colors[irep],linewidth=2)
        plt.title(models[imodel])
        plt.gca().set_xticks(pitch_ids)
        plt.gca().set_xticklabels(pitch_names)
    return fig
    
    
def simulate_resp(inputs,responses=None,model_name='perfect_pitch_440'):
    theo_pitches=np.round(inputs)
    if model_name=='data':
        assert responses is not None
        return responses
    if model_name=='perfect_pitch_440':
        return theo_pitches
    elif model_name=='margot':
        natural_pitches=[0,2,4,5,7,9,11]
        responses=theo_pitches.copy()       
        is_natural=np.array([r in natural_pitches for r in theo_pitches])
        responses[~is_natural]+=np.sign(inputs[~is_natural]-theo_pitches[~is_natural])
        responses[responses==-1]=11
        responses[responses==12]=0
        return responses
#    elif model_name=='margot':
#        natural_pitches=[0,2,4,5,7,9,11]
#        responses=np.zeros(len(inputs))+np.nan        
#        is_natural=np.array([r in natural_pitches for r in theo_pitches])
#        random_draw=(np.random.rand(len(inputs))>0.5)
#        responses[is_natural]=theo_pitches[is_natural]
#        responses[(~is_natural) & random_draw]=theo_pitches[(~is_natural) & random_draw]+1
#        responses[(~is_natural) & (~random_draw)]=theo_pitches[(~is_natural) & (~random_draw)]-1
#        responses[responses==-1]=11
#        responses[responses==12]=0
#        return responses    
    elif model_name=='tone_precision':
        resp_vals=np.array([-2,-1,0,1,2])
        dists=inputs-theo_pitches
        resp_probas=[np.maximum(0.5-0.25*np.abs(dists-r),0) for r in resp_vals]
        cum_probas=np.cumsum(np.array([np.zeros(len(dists))]+resp_probas),axis=0)
        responses=theo_pitches.copy()
        random_draw=np.random.rand(len(inputs))
        choices=np.array([resp_vals[np.histogram([random_draw[ind]],bins=cum_probas[:,ind])[0].astype(bool)] for ind in range(len(inputs))]).flatten()
        responses+=choices
        return responses
        
        
    
def save_data(data_mat,file_name,sep=' '):
    with open(file_name,'w') as f:
        f.write('\n'.join([sep.join([str(val) for val in vec]) for vec in data_mat]))    
        
        
def load_data(file_name,sep=' '):
    with open(file_name,'r') as f:
        lines=f.readlines()
    return np.array([[np.float(txt) for txt in l.split(sep)] for l in lines])
        
if __name__ == "__main__":
    
#    tone_list=range(12)*1
#    oct_lists=[range(-6,10)]
#    ampl_list=[1]*len(oct_lists)
#    tone_dur=0.5
#    
#    for ioct in range(len(oct_lists)):
#        print oct_lists[ioct]
#        shepard_sequence(tone_list,tone_dur,octaves=oct_lists[ioct],ampl_factor=ampl_list[ioct])
#        #shepard_sequence(tone_list,tone_dur,octaves=oct_lists[ioct],ampl_factor=ampl_list[ioct],window_func=shepard_windows['flat'])
##    play_shepard(0,5,ref_freq=ref_freqs['la_3_440'],octaves=[0])
    print circular_correction([7,7,7,7],[4,9,0,1])