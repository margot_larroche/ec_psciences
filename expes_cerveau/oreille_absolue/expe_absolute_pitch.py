#_*_ coding:latin1 _*_


import numpy as np
from time import sleep
import Tkinter as tk
from pitch_funcs import *

####### PARAMETERS ########
max_semitone=12 
sound_dur=1
ampl_factor=0.05
###########################    
    

inputs=[]
responses=[]
pitch_buttons=[]

def loop_func(out):  
    for pb in pitch_buttons:
        pb.config(state='disabled')
    if len(inputs)>0:    
        responses.append(out)
    #sleep(1)
    inputs.append(np.random.uniform()*max_semitone)
#    freq=semitone_to_freq(inputs[-1],min_freq)
#    play_tone(freq,sound_dur,sampling_rate,ampl_func)
    sleep(0.5)
    play_shepard(inputs[-1],sound_dur,ampl_factor=ampl_factor)
    for pb in pitch_buttons:
        pb.config(state='normal')
        
        
pitch_pos=[(0,2),(0,3),(1,4),(2,5),(3,5),(4,4),(5,3),(5,2),(4,1),(3,0),(2,0),(1,1)]
#pitch_pos=[(0,1),(0,2),(1,3),(2,3),(3,3),(4,3),(5,2),(5,1),(4,0),(3,0),(2,0),(1,0)]

window=tk.Tk()
for ipitch in range(len(pitch_names)):
    pitch_buttons.append(tk.Button(window,text=pitch_names[ipitch],command=lambda pid=pitch_ids[ipitch]:loop_func(pid)))
    pitch_buttons[ipitch].grid(row=pitch_pos[ipitch][0],column=pitch_pos[ipitch][1])
    #tk.Button(window,text=pitch_names[ipitch]).grid(row=ipitch,column=0)
tk.Button(window, text='STOP', command=window.destroy).grid(row=7, column=3)    
np.random.seed()    
window.mainloop()  
    
  
file_name=raw_input('nom fichier :')
if len(file_name)>0:
    save_data(np.array([inputs[:-1],responses]).T,file_name+'.txt')