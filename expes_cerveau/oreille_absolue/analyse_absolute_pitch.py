# /!\ A debugger : marche dans spyder mais pas depuis la ligne de commande.

from pitch_funcs import *

data_file='/Users/margot/Code/EC_psciences/expes_cerveau/oreille_absolue/exemples_resultats/resultats_margot_2.txt'
#models=['data','perfect_pitch_440','margot','tone_precision']
models=['data','perfect_pitch_440','margot']
figs=[]

data=load_data(data_file)
figs.append(resp_xy_plot(data[:,0],data[:,1]))
figs.append(distance_hist(data[:,0],data[:,1],models=models))
figs.append(confusion_matrix(data[:,0],data[:,1],models=models))
figs.append(category_hists(data[:,0],data[:,1],bin_width=0.25,models=models))
figs.append(resp_hist(data[:,0],data[:,1],models=models))
figs.append(psychometric_curves(data[:,0],data[:,1],bin_width=0.25,models=models))