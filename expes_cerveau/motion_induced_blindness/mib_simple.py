"""
    Last Edited : 15/10/2020 - Milo Heinrich (MikuoH15TH@gmail.com)
"""

import os
os.environ["SDL_VIDEO_CENTERED"] = "1" # Automatically centers the window (turn it off if you want...)

import pygame
from pygame.locals import *

from math import *

Fullscreen = False
WindowSize = (1000, 600)

Circles      = ((0, 1), ) # Tuple so we can iterate it later
CircleScale  = 0.3
CircleRadius = 8

Grid        = 3
GridSpacing = 0.2
GridLength  = 0.05
GridWidth   = 2

FixRadius       = 8
RotationSpeed   = pi / 2

BackgroundColor = (0, 0, 0)
GridColor       = (100, 100, 255)
CircleColor     = (150, 150, 0)

DisApp_Frames = 1

WindowCenter = (WindowSize[0] / 2.0, WindowSize[1] / 2.0)
WindowScale  = min(WindowSize) / 2.0

def Coord(real):
    """
    Takes real coordinates and returns the pixel coordinates on the screen
    """

    return (int(round(real[0] * WindowScale + WindowCenter[0])), int(round(-real[1] * WindowScale + WindowCenter[1])))

globalCos, globalSin = 1, 0

def SetRotation(angle):
    """
    Sets up rotation
    """

    global globalCos, globalSin
    globalCos, globalSin = cos(angle), sin(angle)

def Rotate(point):
    """
    Rotates a 3D point about the Z-axis by given angle set by SetRotation()
    """

    return (globalCos * point[0] + globalSin * point[1], -globalSin * point[0] + globalCos * point[1])

# graphics initializations
Frames, Show = 0, True
Running = True

# Game loop
try:
    pygame.init()
    
    if Fullscreen:
        Surface = pygame.display.set_mode(WindowSize, HWSURFACE | FULLSCREEN | DOUBLEBUF)
    else:
        Surface = pygame.display.set_mode(WindowSize)

    Clock = pygame.time.get_ticks()

    while Running:
        # event polling
        for event in pygame.event.get():
            if event.type in (QUIT, KEYDOWN): Running = False
            elif event.type == MOUSEBUTTONDOWN: Frames, Show = 0, False
            elif event.type == MOUSEBUTTONUP: Frames, Show = 0, True

        # Rendering
        Surface.fill(BackgroundColor)
        DeltaTime = (pygame.time.get_ticks() - Clock) / 1000.0

        # Maths and drawing
        SetRotation(RotationSpeed * DeltaTime)
        for i in range(-Grid, +Grid + 1):
            for j in range(-Grid, +Grid + 1):
                Center = (GridSpacing * i, GridSpacing * j)

                fr = Rotate((Center[0] - GridLength, Center[1]))
                to = Rotate((Center[0] + GridLength, Center[1]))

                # First line
                pygame.draw.line(Surface, GridColor, Coord(fr), Coord(to), GridWidth)

                fr = Rotate((Center[0], Center[1] - GridLength))
                to = Rotate((Center[0], Center[1] + GridLength))

                # Second line
                pygame.draw.line(Surface, GridColor, Coord(fr), Coord(to), GridWidth)

        for circ in Circles:
            c = (CircleScale * circ[0], CircleScale * circ[1])

            if Show:
                pygame.draw.circle(Surface, CircleColor, Coord(c), CircleRadius, 0)
            else:
                Step = 150 / DisApp_Frames
                Lev = max(0, 150 - Frames * Step)
                Col = (Lev, Lev, 0)
                if Lev > 0:
                    pygame.draw.circle(Surface, Col, Coord(c), CircleRadius, 0)

        pygame.draw.circle(Surface, GridColor, Coord((0, 0)), FixRadius) # Draw the circle
        pygame.display.flip() # Clearing
        Frames += 1

finally: 
    pygame.quit() # Terminates the program