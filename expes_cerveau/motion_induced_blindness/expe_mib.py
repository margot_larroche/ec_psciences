# -*- coding: latin-1 -*-

from os import getcwd
main_path=getcwd()
sep=' '
exp_name='MIB'
fontsize_small=80
fontsize_big=100
font_col=[0,0,0]


import sys, random, pygame
from pygame.locals import *
from math import *
import numpy as np
from datetime import datetime

main_pm = {
'circles' : ((0, 1),),
'circle_scale' : 0.3,
'circle_radius' : 8,
'circle_shape' : 'circle',
'n_grid' : 3,
'grid_spacing' : 0.2,
'grid_length' : 0.05,
'grid_width' : 2,
'fixpoint_radius' : 8,
'fixpoint_shape' : 'circle',
'rotation_speed' : pi/2,
'bg_color' : (0, 0, 0),
'grid_color' : (100, 100, 255),
'circle_color' : (150, 150, 0),
't_max': 100,
'name': 'standard'
}

variants = [
{},
{'name': 'grands_cercles', 'circle_radius': 25, 'fixpoint_radius': 25},
{'name': 'autres_couleurs', 'bg_color': [255,255,255], 'grid_color': [255, 51, 204], 'circle_color': [0, 204, 0]},
{'name': 'carrés', 'circle_shape': 'square', 'fixpoint_shape': 'square'}
]

demo=False
n_repetitions = 2 #10
full_screen = False
window_size = (1024, 768)


window_center = (window_size[0]/2.0, window_size[1]/2.0)
window_scale = min(window_size)/2.0
def coord(real):
	"""takes real coordinates, returns pixel coordinates"""
	return (int(round(real[0]*window_scale + window_center[0])),\
			int(round(-real[1]*window_scale + window_center[1])))

g_cos, g_sin = 1, 0
def set_rotation(angle):
	"""sets up rotation"""
	global g_cos, g_sin
	g_cos, g_sin = cos(angle), sin(angle)
def rotate(point):
	"""rotates a 3D point about the Z-axis by given angle set by set_rotation()"""
	return (g_cos*point[0] + g_sin*point[1], -g_sin*point[0] + g_cos*point[1])

def wait(dur=None,kreturn=False):
	pygame.event.clear()
	ticks0 = pygame.time.get_ticks()
	while True:
		if dur != None and pygame.time.get_ticks() > ticks0 + dur:
			return
		for ev in pygame.event.get():
			if ev.type == QUIT or (ev.type == KEYDOWN and ev.key == K_ESCAPE):
				raise Exception
			if kreturn==True and ev.type == KEYDOWN and ev.key ==K_RETURN:
				return

def win_update(window,fixation_pos=None,object_col=(150,150,150),background_col=(0,0,0),text_big=None,text_small=None):
	window.fill(background_col)
	if fixation_pos<>None:
		pygame.draw.circle(window,object_col,fixation_pos,8)
	if text_big<>None:
		image=font_big.render(text_big, True, object_col, background_col)
		w,h=image.get_size()
		window.blit(image,np.array(coord((0,0)))+np.array([-0.5*w,-2*h]))
	if text_small<>None:
		image=font_small.render(text_small, True, object_col, background_col)
		w,h=image.get_size()
		window.blit(image,np.array(coord((0,0)))+np.array([-0.5*w,2*h]))
	pygame.display.flip()	


# graphics initializations
try:
	# Experiment info
	date=str(datetime.now().date())
	time=str(datetime.now().time())[:8]
	time='-'.join([time[:2],time[3:5],time[6:8]])
	investigator_name=raw_input("\nPRÉNOM EXPÉRIMENTATEUR/TRICE : ")
	subject_name=raw_input("PRÉNOM COBAYE : ")
	if (len(investigator_name)==0) and (len(subject_name)==0):
		filename=''
		demo = True
		n_repetitions=1
		print "\n###################\n /!\ MODE DÉMO /!\ \n###################\n"
	else:
   		filename=main_path+'/'+'_'.join([exp_name,investigator_name,subject_name,date,time])+'.txt'
		with open(filename,'w') as f:
			f.write(sep.join(['Numéro_essai','Version_illusion','Délai_disparition'])+'\n')
	trials=variants * n_repetitions
	random.shuffle(trials)


	pygame.init()
	font_big = pygame.font.Font(None, fontsize_big)
	font_small = pygame.font.Font(None, fontsize_small)
	if full_screen:
		surf = pygame.display.set_mode(window_size, HWSURFACE | FULLSCREEN | DOUBLEBUF)
	else:
		surf = pygame.display.set_mode(window_size)

	for itrial in range(len(trials)):
		win_update(surf, fixation_pos=coord((0,0)), text_big='ESSAI '+str(itrial+1)+'/'+str(len(trials)), text_small='Appuyer sur entree pour demarrer')
		wait(kreturn=True)

		pm=main_pm.copy()
		pm.update(trials[itrial])
		t0 = pygame.time.get_ticks()
		abort=False
		t_disp=-1
		while not(abort):
			for event in pygame.event.get():
				if (event.type==QUIT) or ((event.type == KEYDOWN) and (event.key == K_ESCAPE)): raise Exception()
				elif (event.type == KEYDOWN) and (event.key == K_SPACE): abort,t_disp = True,(pygame.time.get_ticks()-t0)/1000.0


			surf.fill(pm['bg_color'])
			t = (pygame.time.get_ticks() - t0)/1000.0
			if t>pm['t_max']: abort=True
			set_rotation(pm['rotation_speed']*t)

			# Rotating grid
			for i in range(-pm['n_grid'], +pm['n_grid'] + 1):
				for j in range(-pm['n_grid'], +pm['n_grid'] + 1):
					center = (pm['grid_spacing']*i, pm['grid_spacing']*j)
					fr = rotate((center[0] - pm['grid_length'], center[1]))
					to = rotate((center[0] + pm['grid_length'], center[1]))
					pygame.draw.line(surf, pm['grid_color'], coord(fr), coord(to), pm['grid_width'])
					fr = rotate((center[0], center[1] - pm['grid_length']))
					to = rotate((center[0], center[1] + pm['grid_length']))
					pygame.draw.line(surf, pm['grid_color'], coord(fr), coord(to), pm['grid_width'])

			# Remote disappearing objects
			for circ in pm['circles']:
				c = (pm['circle_scale']*circ[0], pm['circle_scale']*circ[1])
				if pm['circle_shape']=='circle':
					pygame.draw.circle(surf, pm['circle_color'], coord(c), pm['circle_radius'], 0)
				elif pm['circle_shape']=='square':
					rect_corner=list(np.array(coord(c))-pm['circle_radius'])
					pygame.draw.rect(surf, pm['circle_color'], pygame.Rect(rect_corner,[2*pm['circle_radius']]*2))

			# Central fixation object
			if pm['fixpoint_shape']=='circle':
				pygame.draw.circle(surf, pm['grid_color'], coord((0, 0)), pm['fixpoint_radius'])
			elif pm['fixpoint_shape']=='square':
				rect_corner=list(np.array(coord((0,0)))-pm['fixpoint_radius'])
				pygame.draw.rect(surf, pm['grid_color'], pygame.Rect(rect_corner,[2*pm['fixpoint_radius']]*2))

			pygame.display.flip()

		if not(demo):
			with open(filename,'a') as f:
				f.write(sep.join([str(itrial+1), pm['name'], str(t_disp)])+'\n')



finally:
	pygame.quit()
