# Motion Induced Blindness Experiences

This is the folder containing experiences for motion induced blindness. They are written in Python.
mib_simple.py works with Python 2 and 3, and requires pygame to be installed.
The other one only works with Python 2 and requires pygame and numpy.
The folder also includes a PDF that explains in detail motion induced blindness.

## How to build:
Download the experience you want, do not forget to update or install pygame (and numpy) if it is not already done.
Then you cant just regularly build it py calling 

```bat
python mib_simple.py
```

## Authors :
    - Margot Larroche
    - Milo Heinrich